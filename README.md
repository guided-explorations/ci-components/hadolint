# hadolint Dockerfile linter

Lints docker files and provides the results in log and as GitLab Code Quality results.

## If This Helps You, Please Star The Original Source Project
One click can help us keep providing and improving this component. If you find this information helpful, please click the star on this components original source here: [Project Details](https://gitlab.com/guided-explorations/ci-components/hadolint/)

## Usage

```yaml
include:
  - component: gitlab.com/guided-explorations/ci-components/hadolint/@<VERSION>
```

where `<VERSION>` is the tag of the version that you are adopting.

### Documentation

[hadolint docs](https://github.com/hadolint/hadolint)

### Inputs and Configuration

| Input | Default value |    Type     | Description |
| ----- | ------------- | ----------- | ----------- |
| `stage` | test | CI Component Input     | The CI Stage to run the component in. |
| `HADOLINT_CONTAINER_TAG` | `v2.12.0-alpine` | CI Component Input     | Pegs container for a stable dependency, but allows override. |
| `HADOLINT_DOCKERFILE` | `Dockerfile` | CI Component Input | The Dockerfile to scan | 


### Debug Tracing

Set either CI_COMPONENT_TRACE or GitLab's global trace (CI_DEBUG_TRACE) to 'true' to get detailed debugging. More info: https://docs.gitlab.com/ee/ci/variables/#enable-debug-logging"

### Working Example Code Using This Component

-  